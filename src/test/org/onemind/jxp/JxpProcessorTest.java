/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import junit.framework.TestCase;
import org.onemind.jxp.parser.ParseException;
/**
 * JxpProcessor unit test
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * 
 */
public class JxpProcessorTest extends TestCase
{

    /** the processor * */
    private JxpProcessor _processor;

    /** the source * */
    private JxpPageSource _source;

    public void setUp() throws Exception
    {
        //_source = new ResourceStreamPageSource("/org/onemind/jxp", "UTF-8"); //eclipse bin
        _source = new FilePageSource("src/test/org/onemind/jxp", "UTF-8"); //eclipse bin
        _processor = new JxpProcessor(new JxpContext(_source));
        _processor.setDoMethodStats(true);
    }

    /** test import and declaration * */
    public void testImportAndDeclaration() throws Exception
    {
        _process("testImportAndDeclaration.jxp");
    }

    /** test the include **/
    public void testInclude() throws Exception
    {
        _process("/included/testInclude.jxp");
    }

    /** test try statement * */
    public void testTry() throws Exception
    {
        _process("testTry.jxp");
    }

    /** test switch statement **/
    public void testSwitch() throws Exception
    {
        _process("testSwitch.jxp");
    }

    /** test function declaration and call **/
    public void testFunction() throws Exception
    {
        _process("testFunction.jxp");
    }
    
    /** test function declaration and call **/
    public void testJsp() throws Exception
    {
        _process("testJsp.jxp");
    }

    /** test statements **/
    public void testPrintStatement() throws Exception
    {
        Map m = new HashMap();
        m.put("test1", "test1");
        m.put("test2", "test2");
        _process("testPrintStatement.jxp", m);
    }

    /** test statements **/
    public void testStatements() throws Exception
    {
        _process("testStatements.jxp");
    }

    /** test array related operation * */
    public void testArray() throws Exception
    {
        _process("testArray.jxp");
    }

    /** test misc control flow statements * */
    public void testControlFlow() throws Exception
    {
        _process("testControlFlow.jxp");
    }

    /** test misc control flow statements * */
    public void testStatic() throws Exception
    {
        Map m = new HashMap();
        m.put("result", new Integer(1));
        _process("testStatic.jxp", m);
        m.put("result", new Integer(2));
        _process("testStatic.jxp", m);
    }

    /** test operators * */
    public void testOperators() throws Exception
    {
        _process("testOperators.jxp");
    }

    /** test synchronize * */
    public void testSynchronized() throws Exception
    {
        _process("testSynchronized.jxp");
    }
    
    public void testParsing() throws Exception
    {
        _process("testParsing.jxp");
    }

    /** test return * */
    public void testReturn() throws Exception
    {
        _process("testReturn.jxp");
    }

    /** test reflection * */
    public void testReflect() throws Exception
    {
        _process("testReflect.jxp");
    }

    /** test jdk15 **/
    public void testJdk15() throws Exception
    {
        _process("testJdk15.jxp");
    }
    
    public void testBufferedPageSource() throws Exception
    {
        ByteArrayPageSource ps = new ByteArrayPageSource();
        JxpContext context = new JxpContext(ps);
        JxpProcessor processor = new JxpProcessor(context);
        ps.putPageBuffer("test", "<% System.out.println(\"a\"); %> ".getBytes());
        processor.process("test", new OutputStreamWriter(System.out));
    }

    public void dumpSyntax(String filename) throws Exception
    {
        _source.getJxpPage(filename).getJxpDocument().dump("");
    }

    /** process the given script */
    private void _process(String filename) throws Exception
    {
        _process(filename, new HashMap());
    }

    /** process the given script */
    private void _process(String filename, Map env) throws Exception
    {
        Writer writer = new OutputStreamWriter(System.out, "UTF-8");
        try
        {
            _processor.process(filename, writer, env);
        } catch (ParseException pe)
        {
            throw pe;
        } catch (Exception e)
        {
            System.out.println("Dumping syntax of " + filename);
            System.out.println(e.getMessage());
            //            /dumpSyntax(filename);
            throw e;
        } finally
        {
            writer.flush();
        }
    }

    
    public void printMethodStats()
    {
        System.out.println(_processor.getMethodStats());
    }

    public static void main(String argsp[]) throws Exception
    {
        JxpProcessorTest test = new JxpProcessorTest();
        test.setUp();
        //        test.testArray();
        //        test.testControlFlow();
        //        test.testFunction();
        //        test.testImportAndDeclaration();
        //        test.testInclude();
        //        test.testOperators();
        //        test.testReflect();
        //        test.testReturn();
        //        test.testStatements();
        //        test.testSwitch();
        //        test.testSynchronized();
        //        test.testTry();
        //test.testPrintStatement();
        test.testParsing();
        test.printMethodStats();
    }
}