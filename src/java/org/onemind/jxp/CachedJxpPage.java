/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp;

/**
 * Represent a cached page source
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * 
 */
public final class CachedJxpPage extends JxpPage
{
    /** the source specific page timestamp **/
    private long _pageTimestamp;
    
    /**
     * {@inheritDoc}
     */
    public CachedJxpPage(JxpPageSource source, String name, String encoding)
    {
        super(source, name, encoding);
    }

    /**
     * Return the timestamp
     * @return the page timestamp.
     */
    public final long getPageTimestamp()
    {
        return _pageTimestamp;
    }

    /**
     * Set the timestamp
     * @param pageTimestamp The timestamp.
     */
    public final void setPageTimepstamp(long pageTimestamp)
    {
        _pageTimestamp = pageTimestamp;
    }
    
    /** 
     * {@inheritDoc}
     */
    public final Object declareStaticVariable(String name, Object value)
    {
        return ((CachingPageSource) getSource()).declarePageStaticVariable(this, name, value);
    }

    /** 
     * {@inheritDoc}
     */
    public final boolean hasStaticVariable(String name)
    {
        return ((CachingPageSource) getSource()).hasPageStaticVariable(this, name);
    }

    /** 
     * {@inheritDoc}
     */
    public final Object getStaticVariable(String name)
    {
        return ((CachingPageSource) getSource()).getPageStaticVariable(this, name);
    }

    /** 
     * {@inheritDoc}
     */
    public Object assignStaticVariable(String name, Object value)
    {
        return ((CachingPageSource) getSource()).assignPageStaticVariable(this, name, value);
    }
}