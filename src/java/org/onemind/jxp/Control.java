/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp;

/**
 * Represent a control object in the script language
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * 
 */
public class Control
{

    /** reference to an object * */
    private Object _obj;

    /** the break control * */
    public static final Control BREAK = new Control();

    /** the continue control * */
    public static final Control CONTINUE = new Control();

    /** the return control * */
    public static final Control RETURN = new Control();
    
    /** the exit **/
    public static final Control EXIT = new Control();

    /**
     * {@inheritDoc}
     */
    private Control()
    {
    }

    /**
     * {@inheritDoc}
     * @param obj an object
     */
    public Control(Object obj)
    {
        _obj = obj;
    }

    /**
     * Get the object
     * @return the object
     */
    public Object getObject()
    {
        return _obj;
    }
}