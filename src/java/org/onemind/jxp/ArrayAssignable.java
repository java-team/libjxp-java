/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp;

import java.lang.reflect.Array;


/**
 * An array wrapper
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * 
 */
public class ArrayAssignable implements Assignable
{
    /** the array object **/
    private Object _arrayObj;
    
    /** the dimension **/
    private int _idx;

    /**
     * Constructor
     * @param dims the dimensions
     */
    public ArrayAssignable(Object arrayObj, int idx)
    {
        _arrayObj = arrayObj;
        _idx = idx;
    }

    /** 
     * {@inheritDoc}
     */
    public Object assign(Object value)
    {        
        Array.set(_arrayObj, _idx, value);
        return value;
    }
}