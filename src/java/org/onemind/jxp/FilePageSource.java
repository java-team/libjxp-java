/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp;

import java.io.*;
import org.onemind.commons.java.util.FileUtils;
import org.onemind.jxp.parser.*;
/**
 * Represents a JxpPageSource using the file system
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * 
 */
public class FilePageSource extends CachingPageSource
{

    /** do modification check **/
    private boolean _modCheck;

    /** the path prefix * */
    private String _pathPrefix;

    /**
     * Constructor
     * @param pathPrefix the prefix
     */
    public FilePageSource(String pathPrefix)
    {
        _pathPrefix = pathPrefix;
    }

    /**
     * Constructor
     * @param pathPrefix the path prefix
     * @param encoding the encoding
     */
    public FilePageSource(String pathPrefix, String encoding)
    {
        this(pathPrefix);
        setEncoding(encoding);
    }

    /**
     * {@inheritDoc}
     */
    public final String getStreamName(String pageName)
    {
        return FileUtils.concatFilePath(getPathPrefix(), pageName);
    }

    /**
     * Return the pathPrefix
     * @return the pathPrefix.
     */
    public final String getPathPrefix()
    {
        return _pathPrefix;
    }

    /**
     * Set the pathPrefix
     * @param pathPrefix The pathPrefix to set.
     */
    public final void setPathPrefix(String pathPrefix)
    {
        _pathPrefix = pathPrefix;
    }

    /**
     * Return whether doing file modification check
     * @return true if modification check is turned on
     */
    public boolean getModCheck()
    {
        return _modCheck;
    }

    /**
     * Set whether to do file modification check
     * @param b true to turn on modification check
     */
    public void setModCheck(boolean b)
    {
        _modCheck = true;
    }

    /** 
     * {@inheritDoc}
     */
    protected boolean isExpired(CachedJxpPage page)
    {
        File f = new File(getStreamName(page.getName()));
        return (page.getPageTimestamp() != f.lastModified());
    }

    /** 
     * {@inheritDoc}
     */
    protected boolean hasStream(String pageName)
    {
        return new File(getStreamName(pageName)).exists();
    }

    /** 
     * {@inheritDoc}
     */
    protected InputStream loadStream(CachedJxpPage page) throws IOException
    {
        File f = new File(getStreamName(page.getName()));
        page.setPageTimepstamp(f.lastModified());
        return new FileInputStream(f);
    }
}