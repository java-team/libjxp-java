/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp.config;

import java.util.HashMap;
import java.util.Map;
import org.onemind.commons.java.datastructure.XmlPropertyElementDigester;
import org.onemind.commons.java.util.ObjectUtils;
import org.onemind.commons.java.xml.digest.*;
import org.onemind.jxp.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
/**
 * JxpConfigDigester digest jxp configuration
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 */
public class JxpConfigDigester extends AbstractElementCreatorDigester implements ElementListener
{

    /** the processor **/
    private JxpProcessor _processor;

    /** the environment **/
    private Map _env;

    /** do method statistics **/
    private boolean _doMethodStats;

    /**
     * Constructor
     */
    public JxpConfigDigester()
    {
        super("jxp");
    }

    /** 
     * {@inheritDoc}
     */
    public void startDigest(SaxDigesterHandler handler, Attributes attr) throws SAXException
    {
        _doMethodStats = ObjectUtils.toBool(attr.getValue("methodstats"), false);
        _env = new HashMap();
        XmlPropertyElementDigester pDig = new XmlPropertyElementDigester("var", _env);
        handler.addSubDigester("env", pDig);
        String pageSourcePath = "pagesources";
        MultiSourceDigester dig = new MultiSourceDigester();
        dig.addListener(this);
        handler.addSubDigester(pageSourcePath, dig);
        FileSourceDigester fdig = new FileSourceDigester();
        fdig.addListener(this);
        handler.addSubDigester(pageSourcePath, fdig);
        StreamSourceDigester sdig = new StreamSourceDigester();
        sdig.addListener(this);
        handler.addSubDigester(pageSourcePath, sdig);
    }

    /** 
     * {@inheritDoc}
     */
    public void objectCreated(Object obj)
    {
        _processor = new JxpProcessor(new JxpContext((JxpPageSource) obj, _env));
        _processor.setDoMethodStats(_doMethodStats);
        setCreatedElement(_processor);
    }

    /**
     * Return the created processor
     * @return the processor
     */
    public JxpProcessor getProcessor()
    {
        return _processor;
    }

    /** 
     * {@inheritDoc}
     */
    public void endDigest(SaxDigesterHandler handler) throws SAXException
    {
        if (_processor == null)
        {
            throw new SAXException("Jxp processor config is missing in the config");
        }
    }
}