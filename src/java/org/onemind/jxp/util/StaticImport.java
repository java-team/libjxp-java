/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp.util;

import java.lang.reflect.*;
import java.util.*;
import java.util.ArrayList;
import java.util.List;
import org.onemind.commons.invoke.AbstractInvocable;
import org.onemind.commons.invoke.AbstractInvocableFunction;
/**
 * StaticImport contains static functions for invocation
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 */
public class StaticImport extends AbstractInvocable
{

    /** the class **/
    private final Class _class;

    /** the static fields **/
    private Map _staticFields;

    /**
     * InvocableFunction which wrap a method
     * @author TiongHiang Lee (thlee@onemindsoft.org)
     */
    private class StaticFunction extends AbstractInvocableFunction
    {

        /** the method **/
        private Method _method;

        /**
         * Constructor
         * @param method the method
         */
        public StaticFunction(Method method)
        {
            super(method.getName(), method.getParameterTypes());
            _method = method;
        }

        /** 
         * {@inheritDoc}
         */
        public Object invoke(Object target, Object[] args) throws Exception
        {
            return _method.invoke(_class, args);
        }
    }

    /**
     * Constructor
     * @param c the class
     */
    public StaticImport(Class c)
    {
        _class = c;
        Method m[] = c.getMethods();
        for (int i = 0; i < m.length; i++)
        {
            int modifier = m[i].getModifiers();
            if ((modifier & Modifier.PUBLIC & Modifier.PUBLIC) > 0)
            {
                addFunction(new StaticFunction(m[i]));
            }
        }
        List l = new ArrayList();
        Field[] fields = _class.getFields();
        for (int i = 0; i < fields.length; i++)
        {
            int modifier = fields[i].getModifiers();
            if ((modifier & Modifier.PUBLIC & Modifier.PUBLIC) > 0)
            {
                try
                {
                    Object obj = fields[i].get(c);
                    if (_staticFields == null)
                    {
                        _staticFields = new HashMap();
                    }
                    _staticFields.put(fields[i].getName(), obj);
                } catch (IllegalAccessException e)
                {
                    //do nothing about it
                }
            }
        }
    }

    /**
     * Get the static fields 
     * @return the static fields
     */
    public final Map getStaticFields()
    {
        if (_staticFields == null)
        {
            return Collections.EMPTY_MAP;
        } else
        {
            return Collections.unmodifiableMap(_staticFields);
        }
    }
}