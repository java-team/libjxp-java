/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp.util;

import java.util.Map;
import org.onemind.commons.java.lang.ref.SoftHashMap;
/**
 * Utilities for discovering static imports
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 */
public class StaticImportUtils
{

    /** the cache for static import **/
    private static Map _cache = new SoftHashMap(100);

    /**
     * Constructor
     */
    private StaticImportUtils()
    {
    }

    /**
     * Get a static import
     * @param c the class 
     * @return the static import
     */
    public static synchronized StaticImport getStaticImport(Class c)
    {
        StaticImport si = (StaticImport) _cache.get(c);
        if (si == null)
        {
            synchronized (c)
            {
                si = (StaticImport) _cache.get(c);
                if (si == null)
                {
                    si = new StaticImport(c);
                    _cache.put(c, si);
                }
            }
        }
        return si;
    }
}
