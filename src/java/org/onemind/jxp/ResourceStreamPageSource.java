/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.onemind.commons.java.util.FileUtils;
/**
 * A JxpResourceSource uses resources from classpath as pages
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * 
 */
public class ResourceStreamPageSource extends CachingPageSource
{

    /** the prefix * */
    private String _pathPrefix;

    /**
     * {@inheritDoc}
     */
    public ResourceStreamPageSource(String pathPrefix)
    {
        _pathPrefix = pathPrefix;
    }

    /**
     * {@inheritDoc}
     */
    public ResourceStreamPageSource(String pathPrefix, String encoding)
    {
        this(pathPrefix);
        setEncoding(encoding);
    }

    /**
     * Get the path prefix
     * @return the path prefix
     */
    public final String getPathPrefix()
    {
        return _pathPrefix;
    }

    /**
     * {@inheritDoc}
     */
    public final String getStreamName(String pageName)
    {
        return FileUtils.concatFilePath(getPathPrefix(), pageName);
    }

    /**
     * Set the pathPrefix
     * @param pathPrefix The pathPrefix to set.
     */
    public final void setPathPrefix(String pathPrefix)
    {
        _pathPrefix = pathPrefix;
    }

    /** 
     * {@inheritDoc}
     */
    
    protected boolean isExpired(CachedJxpPage page)
    {
        return false;
    }

    /** 
     * {@inheritDoc}
     */
    protected boolean hasStream(String pageName)
    {        
        return getClass().getResource(getStreamName(pageName)) != null;
    }

    /** 
     * {@inheritDoc}
     */
    protected InputStream loadStream(CachedJxpPage page) throws IOException
    {
        return getClass().getResourceAsStream(getStreamName(page.getName()));
    }
}