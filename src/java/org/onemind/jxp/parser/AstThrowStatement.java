/* Generated By:JJTree: Do not edit this line. AstThrowStatement.java */

package org.onemind.jxp.parser;

public class AstThrowStatement extends SimpleNode {
  public AstThrowStatement(int id) {
    super(id);
  }

  public AstThrowStatement(JxpParser p, int id) {
    super(p, id);
  }


  /** Accept the visitor. **/
  public Object jjtAccept(JxpParserVisitor visitor, Object data) throws Exception {
    return visitor.visit(this, data);
  }
}
