/* Generated By:JJTree: Do not edit this line. AstUnaryMinusExpression.java */

package org.onemind.jxp.parser;

public class AstUnaryMinusExpression extends SimpleNode
{

    public AstUnaryMinusExpression(int id)
    {
        super(id);
    }

    public AstUnaryMinusExpression(JxpParser p, int id)
    {
        super(p, id);
    }

    /** Accept the visitor. * */
    public Object jjtAccept(JxpParserVisitor visitor, Object data)
            throws Exception
    {
        return visitor.visit(this, data);
    }
}