/* Generated By:JJTree: Do not edit this line. AstLogicalComplementExpression.java */

package org.onemind.jxp.parser;

public class AstLogicalComplementExpression extends SimpleNode
{

    public AstLogicalComplementExpression(int id)
    {
        super(id);
    }

    public AstLogicalComplementExpression(JxpParser p, int id)
    {
        super(p, id);
    }

    /** Accept the visitor. * */
    public Object jjtAccept(JxpParserVisitor visitor, Object data)
            throws Exception
    {
        return visitor.visit(this, data);
    }
}