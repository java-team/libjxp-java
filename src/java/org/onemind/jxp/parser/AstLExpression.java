/* Generated By:JJTree: Do not edit this line. AstLExpression.java */

package org.onemind.jxp.parser;

public class AstLExpression extends SimpleNode
{

    public AstLExpression(int id)
    {
        super(id);
    }

    public AstLExpression(JxpParser p, int id)
    {
        super(p, id);
    }

    /** Accept the visitor. * */
    public Object jjtAccept(JxpParserVisitor visitor, Object data)
            throws Exception
    {
        return visitor.visit(this, data);
    }
}