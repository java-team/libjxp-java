/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp;

import org.onemind.jxp.parser.SimpleNode;
/**
 * A processing exception
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * 
 */
public class ProcessingException extends Exception
{

    /** the page that has problem * */
    private JxpPage _page;

    /** the error node * */
    private SimpleNode _errNode;

    /**
     * Constructor
     * @param page the page that cause the exception
     * @param message the message
     */
    public ProcessingException(JxpPage page, String message)
    {
        super(message);
        _page = page;
    }

    /**
     * Constructor
     * @param page the page that cause the exception
     * @param message the message
     * @param cause the cause
     */
    public ProcessingException(JxpPage page, String message, Throwable cause)
    {
        super(message, cause);
        _page = page;
    }

    /**
     * Constructor
     * @param page the page that cause the exception
     * @param message the message
     * @param cause the cause
     * @param errNode the error node
     */
    public ProcessingException(JxpPage page, String message, Throwable cause, SimpleNode errNode)
    {
        super(message, cause);
        _errNode = errNode;
        _page = page;
    }

    /**
     * Constructor
     * @param page the page that cause the exception
     * @param message the message
     * @param errNode the error node
     */
    public ProcessingException(JxpPage page, String message, SimpleNode errNode)
    {
        super(message);
        _errNode = errNode;
        _page = page;
    }

    /**
     * Constructor
     * @param page the page that cause the exception
     * @param cause the cause
     * @param errNode the error node
     */
    public ProcessingException(JxpPage page, Throwable cause, SimpleNode errNode)
    {
        super(cause);
        _errNode = errNode;
        _page = page;
    }

    /**
     * Get the error node
     * @return the node
     */
    public final SimpleNode getErrNode()
    {
        return _errNode;
    }

    /**
     * Set the error node
     * @param node the node
     */
    public final void setErrNode(SimpleNode node)
    {
        _errNode = node;
    }

    /**
     * {@inheritDoc}
     */
    public final String getMessage()
    {
        if (_errNode != null)
        {
            StringBuffer sb = new StringBuffer();
            if (getCause() != null)
            {
                sb.append(getCause().getMessage());
            } else
            {
                sb.append(super.getMessage());
            }
            sb.append(" at " + _page.getName());
            sb.append(" line " + _errNode.getLine());
            sb.append(", column " + _errNode.getCol());
            return sb.toString();
        } else
        {
            return super.getMessage();
        }
    }
}