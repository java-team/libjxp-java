/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */

package org.onemind.jxp;

import java.io.IOException;
import org.onemind.jxp.parser.AstJxpDocument;
/**
 * Represent the source of JxpPages. It contains multiple JxpPages identified by the ids
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * 
 */
public abstract class JxpPageSource
{

    /**
     * Return whether the jxp page identified by the id exists
     * @param id the id
     * @return true if exists   
     */
    public abstract boolean hasJxpPage(String id);
    
    /**
     * Return a jxp page identified by the id
     * @param id the id
     * @return the jxp page
     * @throws JxpPageNotFoundException if the page cannot be found
     */
    public abstract JxpPage getJxpPage(String id) throws JxpPageNotFoundException;

    /**
     * Print the error source of this page on given line and column
     * @param page the page
     * @param line the line
     * @param col the column
     * @return the StringBuffer
     * @throws IOException if there's IO problem
     */
    public abstract StringBuffer getErrorSource(JxpPage page, int line, int col)
            throws IOException;

    /**
     * Get the jxp document for the JxpPage
     * @param page the JxpPage
     * @return the AstJxpDocument
     * @throws JxpPageSourceException if there's source exception
     */
    public abstract AstJxpDocument getJxpDocument(JxpPage page)
            throws JxpPageSourceException;

}