/*
 * Copyright (C) 2004 TiongHiang Lee
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not,  write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Email: thlee@onemindsoft.org
 */
 
package org.onemind.jxp;



/**
 * An assignable variable
 * @author TiongHiang Lee (thlee@onemindsoft.org)
 * 
 */
public class VariableAssignable implements Assignable
{
    /** the name **/
    private String _name;
    
    /** the context **/
    private JxpProcessingContext _context;
    
    /**
     * Constructor
     * @param name the name 
     * @param context the context
     */
    public VariableAssignable(String name, JxpProcessingContext context)
    {
        _name = name;
        _context = context;
    }
    
    /**
     * Assign to the value
     * @param value the value
     */
    public Object assign(Object value)
    {
        return _context.getNametableStack().assign(_name, value);
    }
}
