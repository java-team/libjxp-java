Source: libjxp-java
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: tony mancill <tmancill@debian.org>
Section: java
Priority: optional
Build-Depends: debhelper-compat (= 13),
               ant
Build-Depends-Indep: default-jdk,
                     ant-optional,
                     junit,
                     libcommons-fileupload-java,
                     libonemind-commons-invoke-java,
                     libonemind-commons-java-java,
                     libservlet3.1-java,
                     xauth,
                     xvfb
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/java-team/libjxp-java
Vcs-Git: https://salsa.debian.org/java-team/libjxp-java.git
Homepage: https://jxp.sourceforge.net/

Package: libjxp-java
Architecture: all
Depends: ${misc:Depends}
Description: Java template engine/script processor
 Jxp (Java scripted page) is a script-processor that process JSP-like files.
 It contains a parser to parse the script file into an abstract syntax tree
 and a tree processor (JxpProcessor) that will process the syntax tree to
 execute the code using reflection API to produce output. The main uses of Jxp
 are:
 .
   * as a script language engine to increase flexibility in the user
     application
   * as a template engine to produce dynamic text output
 .
 Some of the main features of Jxp include:
 .
   * Java as script/template language. Why learn another one? ;)
   * Run JSP-like code outside of servlet container
   * support common java language 1.4 constructs (partial 1.5 syntax support
     on jdk 1.4)
   * support common JSP constructs including import directive, declaration, EL
     etc (taglib not supported, yet)
   * practical template sources management framework
   * support caching of parsed syntax tree to eliminate reparse of template
   * a servlet implementation to enable web-scripting
   * extensible processing context for defining built-in function on the
     scripts
